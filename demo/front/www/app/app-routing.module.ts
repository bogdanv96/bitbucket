import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component'
import {RegisterComponent} from './components/register.component'
import {ConferenceComponent} from './components/conference.component'
import {AddConferenceComponent} from './components/addconference.component'
import {MapComponent} from './components/map.component'

export const routes: Routes = [
    { path: '', redirectTo: 'landing', pathMatch: 'full' },
    { path: 'conference',component:ConferenceComponent},
    { path: 'conference/add',component:AddConferenceComponent},
    { path: 'map' , component:MapComponent},
    { path: 'register',component:RegisterComponent},
    { path: '404', component: NotFoundComponent },
    { path: '**', redirectTo: '/404' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}