import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { LandingModule } from './landing/landing.module';
import { RouterModule } from '@angular/router';
import { NotFoundModule } from './not-found/not-found.module'
import {CommonModule} from '@angular/common';
import { FormsModule} from '@angular/forms';
import {RegisterComponent} from './components/register.component'
import {ConferenceComponent} from './components/conference.component'
import {AddConferenceComponent} from './components/addconference.component'
import {MapComponent} from './components/map.component'

@NgModule({
    imports: [BrowserModule, HttpModule, AppRoutingModule, LandingModule, NotFoundModule,FormsModule,CommonModule],
    declarations: [AppComponent,RegisterComponent,ConferenceComponent,AddConferenceComponent,MapComponent],
    bootstrap: [AppComponent]
})

export class AppModule {
}