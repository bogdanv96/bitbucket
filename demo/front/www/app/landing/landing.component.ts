import {Component, OnInit}      from '@angular/core';
import {AccountService} from '../services/account.service';
import {Router} from '@angular/router'
@Component({
    moduleId: "landing-id", 
    selector: 'app-landing',
    templateUrl: 'app/landing/landing.component.html',
    providers: [AccountService]
})
export class LandingComponent implements OnInit {
    private user:any={}
    private users:any=[]
    constructor(private service: AccountService, private router: Router) {
        
    }

    login(){
        let b:boolean=false;
        this.users.forEach(element => {
            if(element.username==this.user.username && element.password==this.user.password)
            {
                   this.router.navigate(['conference'])
                   b=true;

                   localStorage.setItem("user",JSON.stringify(element));
            }
        });
        if(b==false)
            alert("Invalid credentials");
    }

    getAll(){
        this.service.getAllAccounts().then(x=>{
            this.users=x;
        })
    }
    ngOnInit() {
        this.getAll();
    }
}
