System.register(["@angular/core", "../services/account.service", "@angular/router"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, account_service_1, router_1, RegisterComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (account_service_1_1) {
                account_service_1 = account_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }
        ],
        execute: function () {
            RegisterComponent = class RegisterComponent {
                constructor(router, authService) {
                    this.router = router;
                    this.authService = authService;
                    this.user = {};
                    this.users = [];
                }
                register() {
                    let b = false;
                    if (this.password != this.user.password) {
                        alert("Password doesn't match!");
                        b = true;
                        return;
                    }
                    if (this.user.username == "" || this.user.password == "" || this.user.firstName == "" || this.user.lastName == "") {
                        alert("All fields are required");
                        b = true;
                        return;
                    }
                    this.authService.postAccount(this.user).then(user => {
                    })
                        .catch(error => {
                        // this.router.navigate(['conference']);
                        let ok = false;
                        this.users.forEach(element => {
                            if (element.username == this.user.username) {
                                alert("This username already exists");
                                ok = true;
                            }
                        });
                        if (ok == false) {
                            if (b == false) {
                                alert("You have registered successfully");
                                this.router.navigate(['']);
                            }
                        }
                    });
                }
                getAll() {
                    this.authService.getAllAccounts().then(x => {
                        this.users = x;
                    });
                }
                ngOnInit() {
                    console.log("Register page loaded..");
                    this.getAll();
                }
            };
            RegisterComponent = __decorate([
                core_1.Component({
                    templateUrl: 'app/components/register.component.html',
                    providers: [account_service_1.AccountService]
                }),
                __metadata("design:paramtypes", [router_1.Router, account_service_1.AccountService])
            ], RegisterComponent);
            exports_1("RegisterComponent", RegisterComponent);
        }
    };
});
//# sourceMappingURL=register.component.js.map