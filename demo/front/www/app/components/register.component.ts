import {Component, OnInit}      from '@angular/core';
import {AccountService} from '../services/account.service';
import {Router} from '@angular/router';

@Component({
    templateUrl: 'app/components/register.component.html',
    providers:[AccountService]
})
export class RegisterComponent implements OnInit {
    private user:any={};
    private password:string;
    private users:any=[];

    constructor(private router:Router,private authService:AccountService) {
    }

    register(){
        
        let b:boolean=false;
        if(this.password != this.user.password)
        {
                alert("Password doesn't match!");
                b=true;
                return;
        }
        if(this.user.username==""||this.user.password==""||this.user.firstName==""||this.user.lastName==""){
            alert("All fields are required");
            b=true;
            return;
        }
        this.authService.postAccount(this.user).then(user=>
        {
                
        })
        .catch(error=>{
           // this.router.navigate(['conference']);
           let ok:boolean=false;
            this.users.forEach(element => {
                if(element.username==this.user.username)
                   { alert("This username already exists");ok=true;}
            });
            if(ok==false)
            {
                if(b==false)
                {   
                    alert("You have registered successfully");
                    this.router.navigate(['']); }
            }
        })
    }

    getAll(){
        this.authService.getAllAccounts().then(x=>{
            this.users=x;
        })
    }

    ngOnInit() {
        console.log("Register page loaded..");
        this.getAll();
    }
}