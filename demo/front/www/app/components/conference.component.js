System.register(["@angular/core", "../services/conference.service", "@angular/router"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, conference_service_1, router_1, ConferenceComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (conference_service_1_1) {
                conference_service_1 = conference_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }
        ],
        execute: function () {
            ConferenceComponent = class ConferenceComponent {
                constructor(router, confService) {
                    this.router = router;
                    this.confService = confService;
                    this.allConferences = [];
                    this.conferences = [];
                    this.user = {};
                    this.map = false;
                    this.part = false;
                    this.confClicked = {};
                    this.confClickedP = {};
                    this.participants = [];
                }
                getAll() {
                    this.confService.getAllConferences().then(x => {
                        this.allConferences = x;
                        console.log(x);
                        this.conferences = x;
                    }).catch(e => console.log(e));
                }
                filterConferences() {
                    var select = document.getElementById("filter");
                    var domain = select.options[select.selectedIndex].value;
                    console.log(this.user);
                    if (domain == 'all') {
                        this.conferences = this.allConferences;
                        return;
                    }
                    if (domain == "My") {
                        this.conferences = this.user.conferences;
                        return;
                    }
                    this.conferences = [];
                    this.allConferences.forEach(element => {
                        if (element.domain == domain)
                            this.conferences.push(element);
                    });
                }
                seeParticipants(conf) {
                    this.allConferences.forEach(x => {
                        if (x.name == conf.name)
                            conf = x;
                    });
                    if (this.part == false) {
                        this.participants = conf.accounts;
                        this.confClickedP = conf;
                        this.part = true;
                        if (this.participants == [])
                            this.participants.push("No one joined");
                    }
                    else {
                        this.part = false;
                    }
                }
                add() {
                    this.router.navigate(['conference/add']);
                }
                showMap(conf) {
                    if (this.map == false) {
                        var string = JSON.stringify(conf);
                        localStorage.setItem("conf", string);
                        this.map = true;
                        this.confClicked = conf;
                    }
                    else {
                        this.map = false;
                    }
                }
                ngOnInit() {
                    this.getAll();
                    let string = localStorage.getItem("user");
                    this.user = JSON.parse(string);
                }
            };
            ConferenceComponent = __decorate([
                core_1.Component({
                    templateUrl: 'app/components/conference.component.html',
                    providers: [conference_service_1.ConferenceService]
                }),
                __metadata("design:paramtypes", [router_1.Router, conference_service_1.ConferenceService])
            ], ConferenceComponent);
            exports_1("ConferenceComponent", ConferenceComponent);
        }
    };
});
//# sourceMappingURL=conference.component.js.map