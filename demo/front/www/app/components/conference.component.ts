import {Component, OnInit}      from '@angular/core';
import {ConferenceService} from '../services/conference.service';
import {Router} from '@angular/router';

@Component({
    templateUrl: 'app/components/conference.component.html',
    providers:[ConferenceService]
})
export class ConferenceComponent implements OnInit {
    private allConferences:any=[];
    private conferences:any=[];
    private user:any={}
    private map:boolean=false;
    private part:boolean=false;
    private confClicked:any={};
    private confClickedP:any={};
    private participants:any=[];
    constructor(private router:Router,private confService:ConferenceService) {
    }

    getAll(){
        this.confService.getAllConferences().then(x=>{
            this.allConferences=x
            console.log(x);
            this.conferences=x
        }).catch(e=>console.log(e));
    }
    filterConferences(){
        var select=document.getElementById("filter");
        var domain=select.options[select.selectedIndex].value;
        console.log(this.user);
        if(domain=='all')
            {this.conferences=this.allConferences;
                return;}
        if(domain=="My")
            {
                this.conferences=this.user.conferences;
                return;
            }
        this.conferences=[]
        this.allConferences.forEach(element => {
            if(element.domain==domain)
                this.conferences.push(element)
        });
    }

    seeParticipants(conf:any){
        this.allConferences.forEach(x=> {
            if(x.name == conf.name)
                conf=x;
        })

        if(this.part==false){
            this.participants=conf.accounts;
            this.confClickedP=conf;
            this.part=true;
            if(this.participants==[])
                this.participants.push("No one joined");
        }else{
            this.part=false;
        }
        
    }

    add(){
        this.router.navigate(['conference/add'])
    }

    showMap(conf:any){

        if(this.map==false){
            var string=JSON.stringify(conf)
            localStorage.setItem("conf",string);
            this.map=true
            this.confClicked=conf
        }
        else{
            this.map=false;
        }
    
    }

    ngOnInit() {
        this.getAll();
        let string = localStorage.getItem("user");
        this.user=JSON.parse(string);
    }
}