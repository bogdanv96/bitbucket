import {Component, OnInit}      from '@angular/core';
import {ConferenceService} from '../services/conference.service';
import {Router} from '@angular/router';

@Component({
    templateUrl: 'app/components/addconference.component.html',
    providers:[ConferenceService]
})
export class AddConferenceComponent implements OnInit {
    private conference:any={};
    constructor(private router:Router,private postConferince:ConferenceService) {
    }

    register(){
        
        if(this.conference.nume==""||this.conference.locatie==""||this.conference.domain==""||this.conference.startDate==""
        ||this.conference.endDate==""||this.conference.longitudine==""||this.conference.longitudine=="")
            return;
            console.log(this.conference)
        this.postConferince.postConference(this.conference).then(conference=>
        {
            this.router.navigate(['conference']);    
        })
        .catch(error=>{
            this.router.navigate(['conference']); 
        })
    }

    ngOnInit() {
        console.log("Login page loaded..");
    }
}