System.register(["@angular/core", "@angular/router"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, MapComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }
        ],
        execute: function () {
            MapComponent = class MapComponent {
                constructor(router) {
                    this.router = router;
                }
                initMap() {
                    var conf = JSON.parse(localStorage.getItem("conf"));
                    console.log(conf);
                    var uluru = { lat: conf.latitudine, lng: conf.longitudine };
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 15,
                        center: uluru
                    });
                    var marker = new google.maps.Marker({
                        position: uluru,
                        map: map
                    });
                }
                ngOnInit() {
                    console.log("Map page loaded..");
                    this.initMap();
                }
            };
            MapComponent = __decorate([
                core_1.Component({
                    selector: "map",
                    templateUrl: 'app/components/map.component.html',
                    providers: []
                }),
                __metadata("design:paramtypes", [router_1.Router])
            ], MapComponent);
            exports_1("MapComponent", MapComponent);
        }
    };
});
//# sourceMappingURL=map.component.js.map