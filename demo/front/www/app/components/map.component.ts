import {Component, OnInit}      from '@angular/core';
import {Router} from '@angular/router';

declare var google:any;

@Component({
    selector:"map",
    templateUrl: 'app/components/map.component.html',
    providers:[]
})
export class MapComponent implements OnInit {
    

    constructor(private router:Router) {
    }

    initMap() {
        var conf=JSON.parse(localStorage.getItem("conf"))
        console.log(conf)
        var uluru = {lat: conf.latitudine, lng: conf.longitudine};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }

    ngOnInit() {
        console.log("Map page loaded..");
        this.initMap();
    }
}