package com.example.controller;

import com.example.model.Account;
import com.example.model.AccountConference;
import com.example.model.WebAccount;
import com.example.service.ImplementationService.AccountConferenceService;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Bogdan on 5/12/2017.
 */
@RestController
public class AccountConferenceController {
    AccountConferenceService accountConferenceService;

    public AccountConferenceController() {
        this.accountConferenceService = new AccountConferenceService();
    }


    @RequestMapping(value = "/accounts",method = RequestMethod.POST)
    public Account insertConferenceToAccount(@RequestBody AccountConference body){
        return  accountConferenceService.addConferenceToAccount(body.getIdAccount(),body.getIdConference());
    }
    @RequestMapping(value ="/accounts",method = RequestMethod.PUT)
    public Account deleteConferenceFromAccount(@RequestBody AccountConference body){
        return accountConferenceService.deleteConferenceFromAccount(body.getIdAccount(),body.getIdConference());
    }
}
