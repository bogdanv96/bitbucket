package com.example.controller;

import java.util.List;

import com.example.model.Account;
import com.example.model.WebAccount;
import com.example.service.ImplementationService.AccountService;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletResponse;


@RestController
@EnableWebMvc
public class AccountController {

    AccountService accountService;

    public AccountController() {
        this.accountService = new AccountService();
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public List<WebAccount> list() {
        return accountService.getAllWebAccounts();
    }

    @RequestMapping(value = "/account/{id}", method = RequestMethod.GET)
    public WebAccount account(@PathVariable String id) {
        return accountService.toWebAccount(accountService.getAllAccounts().get(Integer.parseInt(id)));
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/account/{id}", method = RequestMethod.DELETE)
    public WebAccount deleteAccount(@PathVariable String id) {
        return accountService.toWebAccount(accountService.deleteAccount(Integer.parseInt(id)));
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/account/{id}", method = RequestMethod.PUT)
    public WebAccount updateAccount(@PathVariable String id, @RequestBody WebAccount account) {
        Account newAccount = accountService.toDBAccount(account);
        return accountService.toWebAccount(accountService.updateAccount(Integer.parseInt(id), newAccount));
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public WebAccount insertAccount(@RequestBody WebAccount account) {
        return  accountService.toWebAccount(accountService.insertAccount(account.getUsername(), account.getPassword(), account.getFirstName(), account.getLastName()));
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/account/login", method = RequestMethod.POST)
    public HttpServletResponse logIn(@RequestBody WebAccount account){
        return (HttpServletResponse) accountService.toWebAccount(accountService.isOkAccount(account.getUsername(), account.getPassword()));

    }


}
