package com.example.controller;

import com.example.model.Conference;
import com.example.model.WebConference;
import com.example.service.ImplementationService.ConferenceService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Bogdan on 5/12/2017.
 */
@RestController
public class ConferenceController {
    ConferenceService conferenceService;

    public ConferenceController() {
        this.conferenceService = new ConferenceService();
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/conference", method = RequestMethod.GET)
    public List<WebConference> list() {
        return conferenceService.getAllWebConference();
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/conference/{id}", method = RequestMethod.GET)
    public WebConference getConference(@PathVariable String id) {
        return conferenceService.getAllWebConference().get(Integer.parseInt(id));
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/conference/{id}", method = RequestMethod.DELETE)
    public WebConference deleteConference(@PathVariable String id){
        return conferenceService.toWebConference(conferenceService.deleteConference(Integer.parseInt(id)));
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/conference/{id}",method = RequestMethod.PUT)
    public WebConference updateConference(@PathVariable String id, @RequestBody WebConference c){
        return conferenceService.toWebConference(conferenceService.updateConference(Integer.parseInt(id),c));
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/conference", method = RequestMethod.POST)
    public  WebConference insertConference(@RequestBody WebConference c){
        return  conferenceService.toWebConference(conferenceService.insertConference(c));
    }
}
