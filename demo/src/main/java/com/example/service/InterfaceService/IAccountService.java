package com.example.service.InterfaceService;

import com.example.model.Account;
import com.example.model.WebAccount;

import java.util.List;

/**
 * Created by Bogdan on 5/4/2017.
 */
public interface IAccountService {

    public Account isOkAccount(String username, String password);


    public Account insertAccount(String username, String password, String firstName, String lastName);
    public Account deleteAccount(int id);
    public Account updateAccount(int id,Account account);


    public List<Account> getAllAccounts();
    public List<WebAccount> getAllWebAccounts();


    public WebAccount toWebAccount(Account a);
    public Account toDBAccount(WebAccount a);


}
