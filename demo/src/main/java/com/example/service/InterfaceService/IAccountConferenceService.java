package com.example.service.InterfaceService;

import com.example.model.Account;

/**
 * Created by Bogdan on 5/12/2017.
 */
public interface IAccountConferenceService {
    public Account addConferenceToAccount(int idAccount, int idConference);
    public Account deleteConferenceFromAccount(int idAccount, int idConference);
}
