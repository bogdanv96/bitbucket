package com.example.service.InterfaceService;

import com.example.model.Conference;
import com.example.model.WebConference;

import java.util.Date;
import java.util.List;

/**
 * Created by Bogdan on 5/12/2017.
 */
public interface IConferenceService {

    public WebConference toWebConference(Conference conference);
    public Conference toDBConference(WebConference conference);

    public List<Conference> getAllConference();
    public List<WebConference> getAllWebConference();

    public Conference insertConference(WebConference conference);
    public Conference deleteConference(int id);
    public Conference updateConference(int id, WebConference conference);

}
