package com.example.service.ImplementationService;

import com.example.model.Conference;
import com.example.model.WebConference;
import com.example.persistance.ImplementPersistance.ConferencePersistance;
import com.example.persistance.InterfacePersistance.IConferencePersistance;
import com.example.service.InterfaceService.IConferenceService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Bogdan on 5/12/2017.
 */
public class ConferenceService implements IConferenceService {

    ConferencePersistance conferencePersistance;

    public ConferenceService() {
        conferencePersistance = new ConferencePersistance();
    }

    @Override
    public WebConference toWebConference(Conference conference) {
        return new WebConference(conference.getName(),conference.getStartDate(), conference.getEndDate(), conference.getDomain(),conference.getLocatie(),conference.getLatitudine(),conference.getLongitudine());
    }

    @Override
    public Conference toDBConference(WebConference conference) {
        return new Conference(conference.getName(), conference.getStartDate(), conference.getEndDate(),conference.getDomain(), conference.getLocatie(),conference.getLatitudine(),conference.getLongitudine());
    }
    public WebConference toWebConference2(Conference c) {
        return new WebConference(c.getName(), c.getStartDate(), c.getEndDate(), c.getDomain(),c.getLocatie(),c.getLongitudine(),c.getLatitudine(), c.getAccounts());
    }

    @Override
    public List<Conference> getAllConference() {
        return conferencePersistance.getAllConferences();
    }

    @Override
    public List<WebConference> getAllWebConference() {
        List<WebConference> list = new ArrayList<>();
        List<Conference> list1 = getAllConference();
        list1.forEach(x->list.add(toWebConference2(x)));

        return list;
    }

    @Override
    public Conference insertConference(WebConference conference) {
        return conferencePersistance.insertConference(toDBConference(conference));
    }

    @Override
    public Conference deleteConference(int id) {
        return conferencePersistance.deteleConference(id);
    }

    @Override
    public Conference updateConference(int id, WebConference conference) {
        return conferencePersistance.updateConference(id,toDBConference(conference));
    }
}
