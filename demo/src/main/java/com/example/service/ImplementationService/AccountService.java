package com.example.service.ImplementationService;

import com.example.model.Account;
import com.example.model.Conference;
import com.example.model.WebAccount;
import com.example.model.WebConference;
import com.example.persistance.ImplementPersistance.AccountPersistance;
import com.example.service.InterfaceService.IAccountService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by Bogdan on 4/5/17.
 */
@Service("userService")
public class AccountService implements IAccountService {

    AccountPersistance accountPersistance;


    public AccountService() {
        this.accountPersistance = new AccountPersistance();
    }

    @Override
    public Account isOkAccount(String username, String password) {
        return accountPersistance.isOkAccount(username, password);
    }

    @Override
    public Account insertAccount(String username, String password, String firstName, String lastName) {
        Account account = new Account(firstName, lastName, username, password);
        return accountPersistance.insertAccount(account);
    }

    @Override
    public Account deleteAccount(int id) {
        return accountPersistance.deleteAccount(id);
    }

    @Override
    public Account updateAccount(int id, Account account) {
        return accountPersistance.updateAccount(id, account);
    }


    @Override
    public List<Account> getAllAccounts() {
        return accountPersistance.getAllAccounts();
    }

    @Override
    public List<WebAccount> getAllWebAccounts() {
        List<Account> allAccounts = getAllAccounts();
        List<WebAccount> allWebAccounts = new ArrayList<>();
        for (Account a : allAccounts
                ) {
            allWebAccounts.add(toWebAccount(a));
        }
        return allWebAccounts;
    }

    @Override
    public WebAccount toWebAccount(Account a) {
        Set<WebConference> set = new HashSet<WebConference>();
        a.getConferences().forEach(x -> set.add(this.toWebConference(x)));
        return new WebAccount(a.getUserName(), a.getPassWord(), a.getFirstName(), a.getLastName(), set);
    }

    @Override
    public Account toDBAccount(WebAccount a) {
        return new Account(a.getFirstName(), a.getLastName(), a.getUsername(), a.getPassword());
    }

    public WebConference toWebConference(Conference c) {
        return new WebConference(c.getName(), c.getStartDate(), c.getEndDate(), c.getDomain(),c.getLocatie(),c.getLatitudine(),c.getLongitudine());
    }
}
