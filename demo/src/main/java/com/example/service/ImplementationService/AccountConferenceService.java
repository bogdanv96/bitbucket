package com.example.service.ImplementationService;

import com.example.model.Account;
import com.example.model.Conference;
import com.example.persistance.ImplementPersistance.AccountConferencePersistance;
import com.example.persistance.ImplementPersistance.AccountPersistance;
import com.example.persistance.ImplementPersistance.ConferencePersistance;
import com.example.service.InterfaceService.IAccountConferenceService;

import java.util.Set;

/**
 * Created by Bogdan on 5/12/2017.
 */
public class AccountConferenceService implements IAccountConferenceService{

    AccountConferencePersistance accountConferencePersistance;

    public AccountConferenceService() {
        accountConferencePersistance = new AccountConferencePersistance();
    }


    @Override
    public Account addConferenceToAccount(int idAccount, int idConference) {
        return accountConferencePersistance.joinToConference(idAccount,idConference);
    }

    @Override
    public Account deleteConferenceFromAccount(int idAccount, int idConference) {
        return accountConferencePersistance.unjoinToConference(idAccount,idConference);
    }
}
