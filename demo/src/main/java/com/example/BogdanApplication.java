package com.example;

import com.example.model.Conference;
import com.example.model.WebConference;
import com.example.service.ImplementationService.AccountConferenceService;
import com.example.service.ImplementationService.AccountService;
import com.example.service.ImplementationService.ConferenceService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BogdanApplication {

    public static void main(String[] args) {
        SpringApplication.run(BogdanApplication.class, args);
    }
}
