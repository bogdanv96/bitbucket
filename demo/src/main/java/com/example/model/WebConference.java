package com.example.model;

import java.util.Date;
import java.util.Set;

/**
 * Created by Bogdan on 5/10/2017.
 */
public class WebConference {

    private String name;
    private String startDate;
    private String endDate;
    private String domain;
    private String locatie;
    private double longitudine;
    private double latitudine;
    private Set<Account> accounts;


    public WebConference(String name, String startDate, String endDate, String domain, String locatie, double longitudine, double latitudine, Set<Account> accounts) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.domain = domain;
        this.locatie = locatie;
        this.longitudine = longitudine;
        this.latitudine = latitudine;
        this.accounts = accounts;
    }

    public WebConference(String name, String startDate, String endDate, String domain, String locatie, double latitudine, double longitudine) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.domain = domain;
        this.locatie=locatie;
        this.longitudine = longitudine;
        this.latitudine = latitudine;
    }

    public WebConference() {
    }

    public String getLocatie() {
        return locatie;
    }

    public void setLocatie(String locatie) {
        this.locatie = locatie;
    }

    public double getLongitudine() {
        return longitudine;
    }

    public void setLongitudine(double longitudine) {
        this.longitudine = longitudine;
    }

    public double getLatitudine() {
        return latitudine;
    }

    public void setLatitudine(double latitudine) {
        this.latitudine = latitudine;
    }

    public String getName() {
        return name;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getDomain() {
        return domain;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStartDate(String  startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }
}
