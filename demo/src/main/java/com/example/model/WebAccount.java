package com.example.model;

import java.util.Set;

/**
 * Created by Bogdan on 5/8/2017.
 */
public class WebAccount {
    private String username;
    private String password;
    private String firstName;
    private String lastName;

    private Set<WebConference> conferences;

    @Override
    public String toString() {
        return "WebAccount{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    public WebAccount() {
        username=null;
        firstName=null;
        lastName=null;
    }

    public WebAccount(String username, String password, String firstName, String lastName) {

        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public WebAccount(String username, String password, String firstName, String lastName, Set<WebConference> conferences) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.conferences = conferences;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<WebConference> getConferences() {
        return conferences;
    }

    public void setConferences(Set<WebConference> conferences) {
        this.conferences = conferences;
    }
}
