package com.example.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Bogdan on 5/13/2017.
 */
@Entity
@Table(name = "account_conference")
public class AccountConference {
    @Column(name = "account_id")
    private int idAccount;
    @Column(name = "conference_id")
    private int idConference;
    @Id
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public AccountConference(int idAccount, int idConference, int id) {
        this.idAccount = idAccount;
        this.idConference = idConference;
        this.id = id;
    }

    public AccountConference(int idAccount, int idConference) {
        this.idAccount = idAccount;
        this.idConference = idConference;
    }

    public AccountConference() {
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public int getIdConference() {
        return idConference;
    }

    public void setIdConference(int idConference) {
        this.idConference = idConference;
    }

}
