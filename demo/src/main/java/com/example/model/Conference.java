package com.example.model;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Bogdan on 5/8/2017.
 */
@Entity
@Table(name = "Conference")
public class Conference {
    @Id
    private int id;

    @Column(name = "name")
    private String name;
    @Column(name = "startdate")
    private String startDate;
    @Column(name = "enddate")
    private String endDate;
    @Column(name = "domain")
    private String domain;
    @Column(name = "locatie")
    private String locatie;
    @Column(name = "longitudine")
    private double longitudine;
    @Column(name = "latitudine")
    private double latitudine;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Account_Conference", joinColumns = {
            @JoinColumn(name = "conference_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "account_id")
    })
    private Set<Account> accounts = new HashSet<Account>(0);

    public Conference(String name, String startDate, String endDate, String domain,String locatie,double latitudine, double longitudine) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.domain = domain;
        this.locatie = locatie;
        this.longitudine = longitudine;
        this.latitudine = latitudine;
    }
    public Conference() {
    }

    public String getLocatie() {
        return locatie;
    }

    public void setLocatie(String locatie) {
        this.locatie = locatie;
    }

    public double getLatitudine() {
        return latitudine;
    }

    public void setLatitudine(double latitudine) {
        this.latitudine = latitudine;
    }

    public double getLongitudine() {
        return longitudine;
    }

    public void setLongitudine(double longitudine) {
        this.longitudine = longitudine;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }


}
