package com.example.persistance.InterfacePersistance;

import com.example.model.Account;

/**
 * Created by Bogdan on 5/13/2017.
 */
public interface IAccountConferencePersistance {
    public Account joinToConference(int idAccount,int idConference);
    public Account unjoinToConference(int idAccount,int idConference);
}
