package com.example.persistance.InterfacePersistance;

import com.example.model.Account;
import com.example.model.Conference;

import java.util.List;
import java.util.Set;

public interface IAccountPersistance {
    public Account getAccount(String username);
    public Account insertAccount(Account account);
    public Account deleteAccount(int id);
    public Account updateAccount(int id, Account account);
    public Account isOkAccount(String username, String password);
    public List<Account> getAllAccounts();
    public Account getAccount(int id);


    public Set<Conference> getAllConferenceOnAccount(int id);
    public Conference insertConferenceOnAccount(int id, Conference conference);
    public Conference deleteConferenceOnAccount(int id, int idConference);
    public Conference attendConference(int idAccount, int idConference);
}
