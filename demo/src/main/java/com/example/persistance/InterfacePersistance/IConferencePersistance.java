package com.example.persistance.InterfacePersistance;

import com.example.model.Account;
import com.example.model.Conference;

import java.util.List;
import java.util.Set;

/**
 * Created by Bogdan on 5/8/2017.
 */
public interface IConferencePersistance {

    public Conference insertConference(Conference  conference);
    public Conference deteleConference(int id);
    public Conference updateConference(int id, Conference conference);


    public List<Conference> getAllConferences();
    public Conference getConference(int id);


    public Set<Account> getAllAccountOnConference(int id);
    public Account insertAccountOnConference(int id, int idAccount);
    public Account deleteAccountOnConference(int id, int idAccount);

}
