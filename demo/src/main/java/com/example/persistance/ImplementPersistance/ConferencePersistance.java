package com.example.persistance.ImplementPersistance;

import com.example.model.Account;
import com.example.model.Conference;
import com.example.persistance.InterfacePersistance.IConferencePersistance;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Bogdan on 5/8/2017.
 */
public class ConferencePersistance implements IConferencePersistance {
    private static final String PERSISTENCE_UNIT_NAME = "sample";
    private static EntityManagerFactory factory;

    public ConferencePersistance() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    @Override
    public Conference insertConference(Conference conference) {

        EntityManager em = factory.createEntityManager();

        em.getTransaction().begin();
        em.persist(conference);
        em.getTransaction().commit();
        em.close();

        return conference;
    }

    @Override
    public Conference deteleConference(int id) {
        EntityManager em = factory.createEntityManager();

        Conference conference = em.find(Conference.class, id);

        if (conference != null) {
            em.getTransaction().begin();
            em.remove(conference);
            em.getTransaction().commit();
            em.close();
        }

        return conference;
    }

    @Override
    public Conference updateConference(int id, Conference conference) {
        EntityManager em = factory.createEntityManager();

        Conference conference1 = em.find(Conference.class, id);
        if (conference1 != null) {
            if (conference.getName() != "")
                conference1.setName(conference.getName());
            if (conference.getDomain() !="")
                conference1.setDomain(conference.getDomain());
            em.getTransaction().begin();
            em.merge(conference1);
            em.getTransaction().commit();
            em.close();
        }
        return conference1;
    }

    @Override
    public List<Conference> getAllConferences() {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT c from Conference c");
        List<Conference> conferences = query.getResultList();
        return conferences;
    }

    @Override
    public Conference getConference(int id) {
        EntityManager em = factory.createEntityManager();

        Conference conference = em.find(Conference.class,id);

        return conference;
    }

    @Override
    public Set<Account> getAllAccountOnConference(int id) {

        EntityManager em = factory.createEntityManager();

        Conference conference = em.find(Conference.class,id);

        return conference.getAccounts();


    }

    @Override
    public Account insertAccountOnConference(int id, int idAccount) {
        EntityManager em = factory.createEntityManager();

        Account account = em.find(Account.class, idAccount);

        Conference conference = em.find(Conference.class,id);

        conference.getAccounts().add(account);

        return  account;


    }

    @Override
    public Account deleteAccountOnConference(int id, int idAccount) {
        EntityManager em = factory.createEntityManager();

        Conference conference = em.find(Conference.class, id);

        Account account = em.find(Account.class,idAccount);

        conference.getAccounts().remove(account);

        return  account;

    }
}
