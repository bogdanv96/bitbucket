package com.example.persistance.ImplementPersistance;

import com.example.model.Account;
import com.example.model.AccountConference;
import com.example.persistance.InterfacePersistance.IAccountConferencePersistance;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Bogdan on 5/13/2017.
 */
public class AccountConferencePersistance implements IAccountConferencePersistance {
    private static final String PERSISTENCE_UNIT_NAME = "sample";
    private static EntityManagerFactory factory;


    public AccountConferencePersistance() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    private AccountConference getAccountCoference(int idAccount,int idConference){
        EntityManager em = factory.createEntityManager();
        AccountConference accountConference = null;

        em.getTransaction().begin();
        Query query = em.createQuery("SELECT  a from AccountConference  a where a.idAccount=?1 and a.idConference=?2");
        query.setParameter(1, idAccount);
        query.setParameter(2,idConference);
        List<AccountConference> accounts = query.getResultList();
        if (accounts.size() == 0)
            return accountConference;
        accountConference = accounts.get(0);
        em.close();

        return  accountConference;
    }

    @Override
    public Account joinToConference(int idAccount, int idConference) {
        EntityManager em = factory.createEntityManager();

        AccountConference ac = new AccountConference(idAccount,idConference);

        em.getTransaction().begin();
        em.persist(ac);
        em.getTransaction().commit();


        Account account = em.find(Account.class,idAccount);
        em.close();

        return account;
    }

    @Override
    public Account unjoinToConference(int idAccount, int idConference) {


        EntityManager em = factory.createEntityManager();


        AccountConference ac = getAccountCoference(idAccount,idConference);
        System.out.println(ac.toString());
        Account account = null;
        if(ac != null) {
            em.getTransaction().begin();
            em.remove(ac);
            em.getTransaction().commit();
            account = em.find(Account.class,idAccount);
            em.close();
        }

        return  account;
    }
}
