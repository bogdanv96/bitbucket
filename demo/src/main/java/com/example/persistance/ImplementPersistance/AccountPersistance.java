package com.example.persistance.ImplementPersistance;

import com.example.model.Account;
import com.example.model.Conference;
import com.example.persistance.InterfacePersistance.IAccountPersistance;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class AccountPersistance implements IAccountPersistance {
    private static final String PERSISTENCE_UNIT_NAME = "sample";
    private static EntityManagerFactory factory;


    public AccountPersistance() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
    }

    @Override
    public Account getAccount(String username) {

        EntityManager em = factory.createEntityManager();
        Account account = null;


        em.getTransaction().begin();
        Query query = em.createQuery("SELECT  a from Account  a where a.userName=?1");
        query.setParameter(1, username);
        List<Account> accounts = query.getResultList();
        if (accounts.size() == 0)
            return account;
        account = accounts.get(0);
        em.close();

        return account;
    }

    @Override
    public Account insertAccount(Account account) {

        EntityManager em = factory.createEntityManager();

        Account account1 = getAccount(account.getUserName());
        if(account1 != null)
            return account1;
        em.getTransaction().begin();
        em.persist(account);
        em.getTransaction().commit();
        em.close();

        account = getAccount(account.getUserName());
        account.setConferences(new HashSet<>());
        return account;
    }

    @Override
    public Account deleteAccount(int id) {
        EntityManager em = factory.createEntityManager();

        Account account = em.find(Account.class, id);
        account.setConferences(new HashSet<>());
        if (account != null) {
            em.getTransaction().begin();
            em.remove(account);
            em.getTransaction().commit();
            em.close();
        }

        return account;

    }

    @Override
    public Account updateAccount(int id, Account account) {
        EntityManager em = factory.createEntityManager();

        Account account1 = em.find(Account.class, id);
        if (account1 != null) {
            if (account.getFirstName() != "")
                account1.setFirstName(account.getFirstName());
            if (account.getLastName() != "")
                account1.setLastName(account.getLastName());
            if (account.getPassWord() != "")
                account1.setPassWord(account.getPassWord());
            em.getTransaction().begin();
            em.merge(account1);
            em.getTransaction().commit();
            em.close();
        }

        account1.setConferences(new HashSet<>());
        return account1;
    }


    @Override
    public Account isOkAccount(String username, String password) {
        Account account = getAccount(username);
        if (account != null)
            if (account.getPassWord().equals(password))
                return account;
        return new Account();
    }


    @Override
    public List<Account> getAllAccounts() {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Query query = em.createQuery("SELECT a from Account a");
        List<Account> accounts = query.getResultList();
        return accounts;
    }

    @Override
    public Account getAccount(int id) {
        EntityManager em = factory.createEntityManager();

        return em.find(Account.class,id);
    }

    @Override
    public Set<Conference> getAllConferenceOnAccount(int id) {
        EntityManager em = factory.createEntityManager();

        Account account = em.find(Account.class, id);


        return account.getConferences();
    }

    @Override
    public Conference insertConferenceOnAccount(int id, Conference conference) {
        EntityManager em = factory.createEntityManager();

        Account account = em.find(Account.class, id);
        account.getConferences().add(conference);

        em.getTransaction().begin();
        em.merge(account);
        em.getTransaction().commit();
        em.close();

        return conference;
    }


    @Override
    public Conference deleteConferenceOnAccount(int id, int idConference) {
        EntityManager em = factory.createEntityManager();

        Account account = em.find(Account.class, id);
        Set<Conference> conferences = account.getConferences();
        Conference conference = null;
        for (Conference c:conferences
             ) {
            if(c.getId()==idConference)
                conference=c;
        }
        account.getConferences().removeIf(x->x.getId()==idConference);

        em.getTransaction().begin();
        em.merge(account);
        em.getTransaction().commit();
        em.close();

        return conference;
    }

    @Override
    public Conference attendConference(int idAccount, int idConference) {
        EntityManager em = factory.createEntityManager();

        Account account = em.find(Account.class,idAccount);
        Conference conference = em.find(Conference.class,idConference);
        Set<Conference> set= account.getConferences();
        set.add(conference);
        account.setConferences(set);

        return conference;
    }

}
